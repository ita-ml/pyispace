import json
import unittest
from pathlib import Path

import numpy as np
import pandas as pd

from pyispace import train_is


_resource_path = Path(__file__).parent / 'resources'


def read_resource(name):
    return pd.read_csv(_resource_path / (name + '.csv'), index_col='Row')


class TestPreproc(unittest.TestCase):
    metadata = pd.read_csv(_resource_path / 'metadata.csv', index_col='instances')
    with open(_resource_path / 'options.json') as f:
        opts = json.load(f)
    model = train_is(metadata, opts)

    def test_algo_raw(self):
        algo_raw = read_resource('algorithm_raw')
        self.assertTrue(np.allclose(algo_raw.values, self.model.data.Yraw))

    def test_algo_process(self):
        algo_proc = read_resource('algorithm_process')
        self.assertTrue(np.allclose(algo_proc.values, self.model.data.Y, atol=1e-3))

    def test_algo_bin(self):
        algo_bin = read_resource('algorithm_bin')
        self.assertTrue(np.allclose(algo_bin.values, self.model.data.Ybin))

    def test_feat_raw(self):
        feat_raw = read_resource('feature_raw')
        self.assertTrue(np.allclose(feat_raw.values, self.model.data.Xraw))

    def test_feat_process(self):
        feat_proc = read_resource('feature_process')
        self.assertTrue(np.allclose(feat_proc.values, self.model.data.X, atol=1e-3))

    def test_good_algos(self):
        good_algos = read_resource('good_algos')
        self.assertTrue(np.array_equal(good_algos.values, np.reshape(self.model.data.numGoodAlgos, (-1, 1))))

    # def test_portfolio(self):
    #     portfolio = read_resource('portfolio')
    #     self.assertTrue(np.array_equal(portfolio.values, (self.model.data.P + 1).reshape(-1, 1)))

    def test_beta(self):
        beta = read_resource('beta_easy')
        self.assertTrue(np.array_equal(beta.values, self.model.data.beta.astype(int).reshape(-1, 1)))
